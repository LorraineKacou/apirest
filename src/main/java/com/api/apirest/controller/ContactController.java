package com.api.apirest.controller;

import com.api.apirest.entity.Contact;
import com.api.apirest.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/contacts")
public class ContactController {

    private ContactService myContactService;

    public ContactController(ContactService myContactService) {
        this.myContactService = myContactService;
    }

    // create
    // HTTP : POST
    @PostMapping("")
    public Contact createContact(@RequestBody Contact newContact) {
        Contact contactCreatedFromService = myContactService.createOneContact(newContact);

        return contactCreatedFromService;
    }

    // read
    // HTTP : GET ALL MOVIES
    @GetMapping("")
    public List<Contact> getAllContacts() {
        List<Contact> contactListFromService = myContactService.getAllContacts();

        return contactListFromService;
    }



    // HTTP : GET ONE MOVIE BY ID
    @GetMapping("/{contactId}")
    public Optional<Contact> getOneContactById(@PathVariable Long contactId) {
        //Contact oneContactFromService = myContactService.getOneContactById(contactId);

        return myContactService.getOneContactById(contactId);
    }

    // update
    // HTTP : PUT
    @PutMapping("/{contactId}")
    public Contact updateContactById(@PathVariable Long contactId,
                                 @RequestBody Contact updateInfos) {

        Contact updatedContactFromService = myContactService.updateOneContactById(contactId, updateInfos);

        return updatedContactFromService;
    }

    // delete
    // HTTP : DELETE
    @DeleteMapping("/{contactId}")
    public void deleteOnContactById(@PathVariable Long contactId) {
        myContactService.deleteOneContactById(contactId);
    }
}
