package com.api.apirest.service;

import com.api.apirest.entity.Contact;
import com.api.apirest.repository.ContactRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactServiceImpl implements ContactService {
    private ContactRepository contactRepository;

    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Contact createOneContact(Contact newContact) {
        return contactRepository.save(newContact);
    }

    @Override
    public List<Contact> getAllContacts() {
        return  contactRepository.findAll();
    }


    @Override
    public Contact updateOneContactById(Long contactId, Contact updateInfos) {
        var contactupdate= contactRepository.findById(contactId);
        if(contactupdate.isPresent()){
            var currentcontact = contactupdate.get();
            currentcontact.setFirstName(updateInfos.getFirstName());
            currentcontact.setLastName(updateInfos.getLastName());
            currentcontact.setAge(updateInfos.getAge());
            return contactRepository.save(currentcontact);
        }else{
            return null;
        }
    }

    @Override
    public void deleteOneContactById(Long contactId) {
    contactRepository.deleteById(contactId);
    }

    @Override
    public Optional<Contact> getOneContactById(Long contactId) {
        return contactRepository.findById(contactId);
    }
}
