package com.api.apirest.service;


import com.api.apirest.entity.Contact;

import java.util.List;
import java.util.Optional;

public interface ContactService {


    Contact createOneContact(Contact newContact);

    List<Contact> getAllContacts();



    Contact updateOneContactById(Long contactId, Contact updateInfos);

    void deleteOneContactById(Long contactId);

    Optional<Contact> getOneContactById(Long contactId);
}
